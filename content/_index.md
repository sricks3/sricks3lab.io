---
title: Steven's Site
comments: false
---

## Welcome

I'm Steven, and this is my little corner of the Internet. There might or might not be anything of interest to you here, but feel free to look around.

## Why is this site so ugly?

Well first of all, I don't think it's ugly at all. If you're asking why it doesn't have any fancy/modern styling, the answer is because this is more efficient. The goal of this site (at least for now) is to convey information. This essentially unstyled aesthetic is plenty capable of conveying that information, often more efficiently than fancy, modern sites. It's also way easier to create and maintain, way smaller in terms of data storage and transferance over the Internet, and loads much more quickly than most sites with lots of heavy styling.

---