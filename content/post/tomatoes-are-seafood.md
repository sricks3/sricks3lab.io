---
title: 'Tomatoes Are Seafood'
subtitle: ''
date: '2022-06-27'
draft: false
tags: []
author: Steven Ricks
---

**_Beware: This began as a comment on a Hacker News thread. It's also more to do with categorization and musical instruments than tomatoes._**

_String, Woodwind, Brass, Percussion, Keyboard, Voice..._

Classification of musical instruments into these sorts of categories gets tricky in some areas (especially with regard to percussion). Really, the percussion and keyboard categories (when people acknowledge them) kind of become weird catchall categories, especially percussion. Also, depending on our goal, it's ok to let one instrument belong to multiple categories.

**As an exercise:** do you classify based on the means of playing the instrument or by the actual source of the sound?

If it's by the means, then the **piano** is a keyboard instrument, but if by the source of the sound, it's percussion... or is it a string instrument (chordophone) being played by means of percussion which is being triggered by means of keyboard? Is percussion only ever a means of playing? If so, what are **drums**, membrane instruments? And are **cymbals** a different category?

And then what about a **harpsichord**? It's another strung-keyboard instrument, but its strings are plucked rather than hammered. How should it be classified?

And then there's **bass guitar**, clearly an electric chordophone unless it's being slapped in which case it's obviously percussion... but its sound is actually reproduced by a speaker cone at the end of an electrical path that varies in complexity... unless it's being recorded directly to tape/digital without a speaker being used at all. Does the classification of a bass guitar depend on listening format?

It's kind of like when people say, _"Actually, a tomato is a fruit,"_ or, _"There's no such thing as a fish."_ Botanically/taxonomically, sure, those people are absolutely correct, but then a ton of other things that we refer to as vegetables (most of them, off the top of my head) are also technically fruit. In fact, botanically speaking, there's no such thing as a vegetable (huh, kind of like fish... I guess we could consider them seafood).

_To sum things up_, I guess my point is that categorizations are somewhat flexible (in many cases even subjective). More importantly, they're only meaningful when they help us to solve a problem. By extension, they only apply to a particular context. A tomato is both a fruit and a vegetable; you just need to decide whether you're a botanist or a chef.