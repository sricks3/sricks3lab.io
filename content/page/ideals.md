---
title: "Ideals (WIP)"
subtitle: ''
draft: false
tags: []
---

Lately I've been thinking about coming up with a "Rule of Life" or something similar. I've always been very hesitant to state things in absolute or concrete terms, so I've been doing a lot of deliberating. I still haven't come up with anything definitive. Here is a fluctuating and growing list of ideals. This is still very much a work in progress.

- Be kind to everyone all of the time.
- Always respond peacefully.
- Incremental improvement is better than no improvement.
- Strive for a sustainable lifestyle.
  - This is good for a number of climate and socioeconomic reasons, but it's also here because it's good for my quality of life.
- (too vague) Try to save the world.
  - Prefer fair trade goods.
  - Prefer organic foods.
  - What's good for the land is usually good for the farmers, too, and vice versa.
  - Prefer sutainable/durable products.
  - Purchase to meet needs, not wants.
- Avoid junk food.
- Purchase for practicality/utility, not whim/shiny/status.
- Be generous; I have way more than I need (despite how it might feel sometimes).