# stevensite-hugo
---

This is a temporary repo that I'm using to convert [stevensite](https://gitlab.com/sricks3/stevensite) to a Hugo-built site. Hopefully, this will publish to [https://sricks3.gitlab.io](https://sricks3.gitlab.io)